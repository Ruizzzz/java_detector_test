#include <stdio.h>
//#include "src_main_java_operator_Detector.h"
#include "Detector.h"
#include "detection.h"
#include <string>
#include <iostream>

using namespace std;

//global, label\datacfg 20191017
char* _datacfg = NULL;
char** names = NULL;

char* labelpaths = NULL;

jstring stoJstring(JNIEnv* env, char* pat)//change type char* into string
{
    jclass strClass = env->FindClass("Ljava/lang/String;");
    jmethodID ctorID = env->GetMethodID(strClass, "<init>", "([BLjava/lang/String;)V");
    jbyteArray bytes = env->NewByteArray(strlen(pat));
    env->SetByteArrayRegion(bytes, 0, strlen(pat), (jbyte*)pat);
    jstring encoding = env->NewStringUTF("utf-8");
    return (jstring)env->NewObject(strClass, ctorID, bytes, encoding);
}


//jstring to char*
char* jstring2char(JNIEnv* env, jstring gpuid)
{
	  char* rtn = NULL;
	  jclass clsstring = env->FindClass("java/lang/String");
	  jstring strencode = env->NewStringUTF("utf-8");
	  jmethodID mid = env->GetMethodID(clsstring, "getBytes", "(Ljava/lang/String;)[B");
	  jbyteArray barr= (jbyteArray)env->CallObjectMethod(gpuid, mid, strencode);
	  jsize alen = env->GetArrayLength(barr);
	  jbyte* ba = env->GetByteArrayElements(barr, JNI_FALSE);
	  if (alen > 0)
	  {
	    rtn = (char*)malloc(alen + 1);
	    memcpy(rtn, ba, alen);
	    rtn[alen] = 0;
	  }
	  env->ReleaseByteArrayElements(barr, ba, 0);
	  return rtn;
}



//JNIEXPORT jlong JNICALL Java_src_main_java_operator_Detector_initialize// 3-1: initialize(), return (jlong)peer.
JNIEXPORT jlong JNICALL Java_Detector_initialize// 3-1: initialize(), return (jlong)peer.
  (JNIEnv *env , jobject obg , jstring cfgfile, jstring weightfile, jstring datacfg, jstring labelpath, jint batchsize , jint gpu_index , jstring gpuid){

        //printf("************************************************ This is the 1st GetStringUTFChars...\n");

        //printf("before ************************************************ cfgfile: %s \n", cfgfile);
        char* _cfgfile = (char*)(env)->GetStringUTFChars(  cfgfile, 0 );
	//printf("after  ************************************************ cfgfile: %s \n", _cfgfile);

        //printf("************************************************ This is the 2nd GetStringUTFChars...\n");
        char* _weightfile = (char*)(env)->GetStringUTFChars( weightfile, 0 );

	//printf("Java_Detector_initialize in cpp.\n");

        char* gpuids =jstring2char(env, gpuid);



	network* peer = load_network_test(_cfgfile, _weightfile , batchsize , gpu_index , gpuids);

	(env)->ReleaseStringUTFChars(  cfgfile, _cfgfile );
	(env)->ReleaseStringUTFChars(  weightfile, _weightfile );

	// label\datacfg 20191017
	_datacfg = (char*)(env)->GetStringUTFChars( datacfg, 0 );
	names = load_names(_datacfg);

        labelpaths =jstring2char(env, labelpath);


	return (jlong)peer;
}






//JNIEXPORT jbyteArray JNICALL Java_src_main_java_operator_Detector_jpg2Bytes
JNIEXPORT jbyteArray JNICALL Java_Detector_jpg2Bytes
  (JNIEnv *env, jobject obj, jstring path, jint w, jint h, jint c){

      //printf("************************************************ This is the 3rd GetStringUTFChars...\n");
      char* _path = (char*)(env)->GetStringUTFChars(  path, 0 );
      unsigned char * bytes = jpg2BytesInC(_path, c);
      //printf("Detector.cpp bytes[1] %d.\n", bytes[1]);

	//C++中的BYTE[]转jbyteArray
	//nOutSize是BYTE数组的长度  BYTE pData[]
         //int nOutSize = sizeof(bytes)/sizeof(unsigned char);
         int nOutSize = w * h * c;

	//printf("size of bytes: %ld.\n",sizeof(bytes));
       // printf("size of unsigned char: %ld.\n", sizeof(unsigned char));
	//printf("whc, nOutSize: %d.\n", nOutSize);

	jbyte *by = (jbyte*)bytes;
	jbyteArray jarray = (env)->NewByteArray(nOutSize);
	(env)->SetByteArrayRegion(jarray, 0, nOutSize, by);

      //printf("jarray[0] %c.\n", jarray[0]);
      return jarray;

}




//JNIEXPORT jobjectArray JNICALL Java_Detector_computeBoxesAndAccByInputBytes
//  (JNIEnv *, jobject, jlong, jstring, jbyteArray, jstring, jfloat, jfloat, jint, jint, jint, jint);

//JNIEXPORT jobjectArray JNICALL Java_src_main_java_operator_Detector_computeBoxesAndAccByInputBytes// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
JNIEXPORT jobjectArray JNICALL Java_Detector_computeBoxesAndAccByInputBytes// 3-4: computeBoxesAndAccByInputBuffer(),  return infos.
  (JNIEnv *env, jobject obj, jlong structWrapperPeer, jbyteArray bytes, jstring outfile,jfloat thresh,jfloat hier_thresh,
  jint fullscreen,jint ww, jint hh, jint c){

        //printf("computeBoxesAndAccByInputBytes in cpp.\n");

	network* net = (network*)structWrapperPeer;

	// bs setting...
	//set_batch_network(net, bs);

	//printf("************************************************ This is the 4th GetStringUTFChars...\n");

	//printf("before ************************************************ outfile:  %s\n", outfile);

  	char* _outfile = (char*)(env)->GetStringUTFChars( outfile, 0 );

	//printf("after  ************************************************ outfile:  %s\n", _outfile);



	//printf("************************************************ This is the 5th GetStringUTFChars...\n");




  	jobjectArray infos = NULL;
	jsize len;
	int i;


	//float * bufferFloat = (float*) malloc(sizeof(float)*w*h*c);
	//jfloat * _buffer = (jfloat*) (env)->GetFloatArrayElements(buffer,NULL);

	//unsigned char * _bytes = (unsigned char*)bytes;

        //JNI jbyteArray转char*
	unsigned char *_bytes = NULL;
	jbyte *bytess = (env)->GetByteArrayElements(bytes, 0);
        int chars_len = (env)->GetArrayLength(bytes);
	_bytes = new unsigned char[chars_len + 1];
	memset(_bytes,0,chars_len + 1);
	memcpy(_bytes, bytess, chars_len);
	_bytes[chars_len] = 0;

	//JNI jstring转char*   方法1



          //printf("label path rtn:  %s\n", rtn);

	//另一种方法  方法2
       // char *nativeString = (env)->GetStringUTFChars(env, labelpath);
       //char *nativeString = env->GetStringUTFChars(labelpath, JNI_FALSE);

      // use your string

        //printf("before detectByInputBytes in cpp.\n");

	boxesAndAcc* bTest =  detectByInputBytes(_bytes, thresh, 0.5, _outfile, fullscreen, names, net, ww, hh, c, labelpaths);

         //printf("after detectByInputBytes in cpp.\n");

	//free(bufferFloat);

	//方法1
        //env->ReleaseByteArrayElements(barr, ba, 0);
        //方法2
        // (env)->ReleaseStringUTFChars(labelpath, nativeString, 0);
       // env->ReleaseStringUTFChars(labelpath, nativeString);



	(env)->ReleaseByteArrayElements(bytes, bytess, 0);

	jclass jboxesAndAcc = NULL;
	//printf("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII    jboxesAndAcc %d.\n", jboxesAndAcc);


        // 这个类没有找到。。。。。。。20191013
	//jboxesAndAcc = env->FindClass("src/main/java/operator/BoxesAndAcc");
	jboxesAndAcc = env->FindClass("BoxesAndAcc");


	len = bTest[0].size;
	int infosLen = 3;
	//printf("fuck beeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n");

        //printf("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIii len: %d\n", len);
        //printf("IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII    jboxesAndAcc %d.\n", jboxesAndAcc);

	infos = env->NewObjectArray(len, jboxesAndAcc, NULL);
	//printf("fuck heeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee\n");

	for(i = 0; i < len; i++)
	{
		if(bTest[i].isVaild){

		jclass jBox = NULL;
                // 同样的问题。。。。。。20191013
		//jBox = env->FindClass("src/main/java/operator/Box");
		jBox = env->FindClass("Box");
		jobject jBoxObj;
		jfieldID x;
		jfieldID y;
		jfieldID w;
		jfieldID h;
		x = env->GetFieldID(jBox, "x", "F");
		y = env->GetFieldID(jBox, "y", "F");
		w = env->GetFieldID(jBox, "w", "F");
		h = env->GetFieldID(jBox, "h", "F");
		jmethodID jBoxInit = env->GetMethodID(jBox,"<init>","()V");
		jBoxObj = env->NewObject(jBox,jBoxInit);

		env->SetFloatField(jBoxObj, x, (jfloat)bTest[i].boxes.x);
		env->SetFloatField(jBoxObj, y, (jfloat)bTest[i].boxes.y);
		env->SetFloatField(jBoxObj, w, (jfloat)bTest[i].boxes.w);
		env->SetFloatField(jBoxObj, h, (jfloat)bTest[i].boxes.h);

		jobject jboxesAndAccObj;
		jfieldID names;
		jfieldID size;
		jfieldID acc;
		jfieldID isVaild;
		jfieldID boxes;
 		//printf("************************************************computeBoxesAndAccByInputBytes in cpp.\n");

		names = env->GetFieldID(jboxesAndAcc, "names", "Ljava/lang/String;");

 		//printf("################################################computeBoxesAndAccByInputBytes in cpp.\n");

		size = env->GetFieldID(jboxesAndAcc, "size", "I");
	        if (names == NULL) {
	        	 cout<<"load names error"<<endl; /* failed to find the field */
		}

		acc = env->GetFieldID(jboxesAndAcc, "acc", "F");
		isVaild = env->GetFieldID(jboxesAndAcc, "isVaild", "Z");
                // 同样的问题。。。。。。20191013
		//boxes = env->GetFieldID(jboxesAndAcc, "boxes", "Lsrc/main/java/operator/Box;");
		boxes = env->GetFieldID(jboxesAndAcc, "boxes", "LBox;");


		jmethodID jboxesAndAccInit = env->GetMethodID(jboxesAndAcc,"<init>","()V");

		jboxesAndAccObj = env->NewObject(jboxesAndAcc,jboxesAndAccInit);


		env->SetObjectField(jboxesAndAccObj, names, stoJstring(env,bTest[i].names));
		env->SetIntField(jboxesAndAccObj, size, (jint)(bTest[i].size));
		env->SetFloatField(jboxesAndAccObj, acc, (jfloat)(bTest[i].acc));
		env->SetBooleanField(jboxesAndAccObj, isVaild, (jboolean)(bTest[i].isVaild));
		env->SetObjectField(jboxesAndAccObj, boxes, jBoxObj);

		jstring string=(jstring)(env->GetObjectField(jboxesAndAccObj, names));

		env->SetObjectArrayElement(infos,i, jboxesAndAccObj);

 		//JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJj 20191012
	        env->DeleteLocalRef(jBoxObj);


		}else{
			jobject jboxesAndAccObj;
			jmethodID jboxesAndAccInit = env->GetMethodID(jboxesAndAcc,"<init>","()V");
			jboxesAndAccObj = env->NewObject(jboxesAndAcc,jboxesAndAccInit);
			env->SetObjectArrayElement(infos,i, jboxesAndAccObj);


 		//JJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJJj 20191012
	        env->DeleteLocalRef(jboxesAndAccObj);

		}

	}
	if(bTest!=NULL){
		free(bTest);
	}


  (env)->ReleaseStringUTFChars(  outfile, _outfile );

       // (env)->ReleaseStringUTFChars(  datacfg, _datacfg );
  	//(env)->ReleaseStringUTFChars(  labelpath, labelpaths );


 // (env)->ReleaseFloatArrayElements(buffer,_buffer,0);
  return infos;

  //env->DeleteLocalRef(infos);


}

